import { render, waitFor, screen, within } from '@testing-library/react';
import App from './App';

test('table renders with headers', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const uuidHeader = await waitFor(() => screen.getByText(/Uuid/g));
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

test('users without not qualified for payment have disabled buttons', async () => {
  render(<App />)
  await waitFor(() => screen.getByRole('table')) 
  // Forgive this very brittle test 
  // I'm not the most familiar with react-testing lib. (but I know how to write test I promise ;])
  const teRequest = await waitFor(() => screen.getByTestId("tony-earley-requestedamount"))
  const teCell = screen.getByTestId("tony-earley-initiatepayment")
  const teBtn = within(teCell).getByRole("button")
  expect(teRequest).toBeEmptyDOMElement()
  expect(teCell).toBeInTheDocument()
  expect(teBtn).toHaveAttribute('disabled')
})