import React, { useState, useEffect, useCallback } from "react";
import "./App.css";
import Table from "./components/table.jsx";
import { Container, Button } from "@material-ui/core";
import formatCurrency from "./utils/formatCurrency";
import { getUsers } from "./services/users.js";
import { getApplications } from "./services/applications.js";
import { getPayments, createPayment } from "./services/payments.js";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

const App = () => {
  /**
   * Hydrate data for the table and set state for users, applications, and payments
   */
  const [users, setUsers] = useState([]);
  const [applications, setApplications] = useState([]);
  const [payments, setPayments] = useState([]);
  const [tableData, setTableData] = useState([]);
  const [filter, setFilter] = useState(false);

  const initiatePayment = useCallback(
    async ({ applicationUuid, requestedAmount }) => {
      const { body } = await createPayment({
        applicationUuid,
        requestedAmount,
      });
      setPayments([...payments, body]);
    },
    [payments]
  );

  useEffect(() => {
    async function fetchData() {
      const [usersData, applicationsData, paymentsData] = await Promise.all([
        getUsers(),
        getApplications(),
        getPayments(),
      ]);

      setUsers(usersData.body);
      setApplications(applicationsData.body);
      setPayments(paymentsData.body);
    }
    fetchData();
  }, []);

  useEffect(() => {
    // Handling the data formatting for the table this way lets react help out more with rendering
    // now we don't need to rely on the `dataLoaded` state and we can avoid some unnecessary re-renders, bonus for devs and users

    // we can take this a step further by memoizing the functions looking application matches and payment matches but
    // i'll skip that for sake of time
    let tempData = users.flatMap(({ uuid, name, email }) => {
        const { requestedAmount, uuid: applicationUuid } =
          applications.find((application) => application.userUuid === uuid) || {};

        const { paymentAmount, paymentMethod } =
          payments.find(
            (payment) => payment.applicationUuid === applicationUuid
          ) || {};

        if (filter && paymentAmount) {
          return [];
        }

        return {
          uuid,
          name,
          email,
          requestedAmount: formatCurrency(requestedAmount),
          paymentAmount: formatCurrency(paymentAmount),
          paymentMethod,
          initiatePayment: !paymentAmount ? (
            <Button
              onClick={() =>
                initiatePayment({
                  applicationUuid,
                  requestedAmount,
                })
              }
              disabled={!requestedAmount}
              variant="contained"
            >
              Pay
            </Button>
          ) : null,
        };
      });
    setTableData(tempData);
  }, [payments, applications, users, filter, initiatePayment]);

  return (
    <div className="App">
      <Container>
        <FormControlLabel
          control={
            <Switch
              checked={filter}
              onChange={() => setFilter((filter) => !filter)}
              name="exclude paid"
              color="primary"
            />
          }
          label="Exclude Already Paid"
        />
        <Table data={tableData} />
      </Container>
    </div>
  );
};

export default App;
