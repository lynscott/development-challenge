

# Third Challenge
Congratulations ⚔️, you're now the hero of the Customer Success Team and other developer
because of your excellent testing chops. 🙌

The dashboard is function just fine but with so many people applying for funds, it's getting a little slow to load all of the records in each time. The Customer Success Manager comes to you and asks if there's anything that can be done to speed it up. You both decide that adding pagination to the table/API should do the trick.

### Bonus modifications:
- `material-ui` also supports sorting within their library, add sorting
to the table as well so that you can sort by students without payments so there's even less scrolling/page changes.
- Make a proposal: there are a million ways to improve a codebase. Propose a change that could be made to the application that would make it better for either the developer or the "customer" and explain the impact that it would have.

#### Proposals
I wasn't sure if these proposal could be written or not but I don't want to spend hours implementing them. So I'll write!

- TypeScript on the UI: there's a good bit of logic and data manipulation going on UI side and typescript would help
a lot with catching bugs and general self-documentation there. I was tempted to do it here with just these few files but I know this would be a much harder integration for large codebase.

- Api logic in UI?: speaking of the logic and data manipulation in the UI, I would push that api side. This would help a ton with pagination from the server and just generally keep the UI from working too hard where apis and db queries shine. 
In this case with dummy data I would pull those queries in each of the api calls (users, payments, applications) into one call,
format that into a central array of objects with just what the table needs on it // users `{uuid, name, email, requestedAmount, paymentAmount, paymentMethod }`. Then UI side we can feed that directly into the table. Less hassle and much easier to add new features or filters to. Also we could paginate that array & stream each block of pages to the frontend with json string conversion. e.g. First page we stream the first 50 results and stop. user clicks next page then stream the next 50. Depending on how quickly we want users to see new data.